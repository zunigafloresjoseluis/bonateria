var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Producto=require('../models/producto');
var router = express.Router();


router.get('/', function (req, res) {
    res.render('index', { user : req.user });
});
router.get('/lenceria', function (req, res) {
    res.render('mujer', { user : req.user });
});


router.get('/register', function(req, res) {
    res.render('register', { });
});

router.post('/register', function(req, res) {
    Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
        if (err) {
            return res.render('register', { account : account });
        }

        passport.authenticate('local')(req, res, function () {
            res.redirect('/');
        });
    });
});

router.get('/login', function(req, res) {
    res.render('login', { user : req.user });
});

router.post('/login', passport.authenticate('local'), function(req, res) {
    res.redirect('/lenceria');
});



router.post('/agregaproducto',function(req,res,next){
    var P= new Producto({ id_producto : req.body.id_producto,
    nombre:req.body.nombre,
    tipo:req.body.tipo,
    marca:req.body.marca,
    color:req.body.color,
    descrip:req.body.descrip });
    P.save(function(err,P){
        if(err){return next(err)}
        res.redirect('mujer');
    })
})

router.get('/Lenceria/mujer', function(req, res) {
  res.render('mujer',{ user : req.user });
});
router.get('/AgregarProducto', function(req, res) {
    res.render('AgregarP', {  user : req.user,title:'AgregarProducto'});
});
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

router.get('/ping', function(req, res){
    res.status(200).send("pong!");
});

module.exports = router;
