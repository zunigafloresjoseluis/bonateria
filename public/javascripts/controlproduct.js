angular.module('appProducto',['ui.router'])
  
  .controller('agregaProducto',function($scope){
      $scope.producto={}
      $scope.productos=[];
      $scope.agregar=function(){
          $scope.productos.push({
              id_producto:$scope.producto.id_producto,
              nombre:$scope.producto.nombre,
              tipo:$scope.producto.tipo
          })

          $scope.producto.id_producto='';
          $scope.producto.nombre='';
          $scope.producto.tipo='';
      }
  })